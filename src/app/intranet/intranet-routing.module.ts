import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntranetViewComponent } from './containers/intranet-view/intranet-view.component';

const routes: Routes = [
  {
    path: '',
    component: IntranetViewComponent,
    children: [
      {
        path: '',
        loadChildren: '../home/home.module#HomeModule'
      },
      {
        path: 'posts',
        loadChildren: '../posts/posts.module#PostsModule'
      },
      {
        path: 'users',
        loadChildren: '../users/users.module#UsersModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntranetRoutingModule {}
