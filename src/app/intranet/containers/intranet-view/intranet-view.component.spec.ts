import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntranetViewComponent } from './intranet-view.component';

describe('IntranetViewComponent', () => {
  let component: IntranetViewComponent;
  let fixture: ComponentFixture<IntranetViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntranetViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntranetViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
