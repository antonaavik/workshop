import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { UsersService } from 'src/app/shared/services/users.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-intranet-view',
  template: `
    <p *ngIf="user$ | async as user">
      Hello {{ user.firstName }}! <a href="#" (click)="logout($event)">Logout</a>
    </p>
    <ul>
      <li><a href="#" routerLink="/">Home</a></li>
      <li><a href="#" routerLink="/posts">Posts</a></li>
      <li><a href="#" routerLink="/users">Users</a></li>
    </ul>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./intranet-view.component.css']
})
export class IntranetViewComponent implements OnInit {
  user$: Observable<User>;

  constructor(private authService: AuthService, private usersService: UsersService) {}

  ngOnInit() {
    const userId = this.authService.getUserId();
    this.user$ = this.usersService.getUser(userId);
  }

  logout(e) {
    e.preventDefault();
    this.authService.logout();
  }
}
