import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-post',
  template: `
    <app-card [cover]="post.cover" (click)="showPost(post.id)">
      <main>
        <h2>{{ post.title }}</h2>
        <app-authors [authors]="post.authors"></app-authors>
        <p>{{ post.body }}</p>
      </main>
    </app-card>
  `,
  styleUrls: ['./card-post.component.css']
})
export class CardPostComponent implements OnInit {
  @Input() post: Post;

  constructor(private router: Router) {}

  ngOnInit() {}

  showPost(postId: string) {
    this.router.navigate(['/posts', postId]);
  }
}
