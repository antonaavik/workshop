import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-authors',
  template: `
    <p>
      Written by
      <span *ngFor="let author of authors; last as isLast; first as isFirst">
        <span *ngIf="!isFirst && !isLast && authors.length > 1">, </span>
        <span *ngIf="isLast && authors.length > 1"> and </span>
        <a href="#" [routerLink]="'/users/' + author.id">{{ author | fullName }}</a>
      </span>
    </p>
  `,
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  @Input() authors: User[];
  constructor() {}

  ngOnInit() {}
}
