import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div class="card__cover" *ngIf="cover">
        <img [src]="cover" alt="" />
      </div>
      <div class="card__content"><ng-content select="main"></ng-content></div>
      <div class="card__footer"><ng-content select="footer"></ng-content></div>
    </div>
  `,
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() cover: string;
  constructor() {}

  ngOnInit() {}
}
