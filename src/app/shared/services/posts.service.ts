import { environment } from '@app/env';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  constructor(private http: HttpClient) {}

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${environment.API_URL}/posts?_embed=comments`);
  }

  getPost(postId: string): Observable<Post> {
    return this.http.get<Post>(`${environment.API_URL}/posts/${postId}?_embed=comments`);
  }
}
