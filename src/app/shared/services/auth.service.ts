import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private router: Router) {}

  login(userId: string) {
    localStorage.setItem('user', userId);
    this.router.navigate(['/']);
  }

  logout() {
    localStorage.removeItem('user');
    this.router.navigate(['auth']);
  }

  getUserId() {
    return localStorage.getItem('user');
  }

  isLoggedIn(): boolean {
    return !!this.getUserId();
  }
}
