import { environment } from '@app/env';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.API_URL}/users`);
  }

  getUser(userId: string): Observable<User> {
    return this.http.get<User>(`${environment.API_URL}/users/${userId}`);
  }
}
