import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';
import { CardPostComponent } from './components/card-post/card-post.component';
import { FullNamePipe } from './pipes/full-name.pipe';
import { AuthorsComponent } from './components/authors/authors.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [CardComponent, FullNamePipe, CardPostComponent, AuthorsComponent],
  imports: [CommonModule, RouterModule],
  exports: [CardComponent, FullNamePipe, CardPostComponent, AuthorsComponent]
})
export class SharedModule {}
