import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fullName'
})
export class FullNamePipe implements PipeTransform {
  transform(user: User, args?: any): any {
    console.log(user);
    return `${user.firstName} ${user.lastName}`;
  }
}
