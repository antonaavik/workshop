import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsViewComponent } from './containers/posts-view/posts-view.component';
import { PostViewComponent } from './containers/post-view/post-view.component';

const routes: Routes = [
  {
    path: '',
    component: PostsViewComponent
  },
  {
    path: ':postId',
    component: PostViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule {}
