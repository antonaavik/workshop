import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService } from '../../../shared/services/posts.service';

@Component({
  selector: 'app-posts-view',
  template: `
    <h1>Posts</h1>
    <div *ngFor="let post of posts$ | async">
      <app-card-post [post]="post"></app-card-post>
    </div>
  `,
  styleUrls: ['./posts-view.component.css']
})
export class PostsViewComponent implements OnInit {
  posts$: Observable<Post[]>;

  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.posts$ = this.postsService.getPosts();
  }
}
