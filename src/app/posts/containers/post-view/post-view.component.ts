import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

import { PostsService } from '../../../shared/services/posts.service';

@Component({
  selector: 'app-post-view',
  template: `
    <div *ngIf="this.post$ | async as post">
      <h1>{{ post.title }}</h1>
      <app-authors [authors]="post.authors"></app-authors>
      <p>{{ post.body }}</p>
      <div *ngFor="let comment of post.comments">
        <hr />
        <p>
          {{ comment.body }} <br />
          <small>- {{ comment.author.firstName }} {{ comment.author.lastName }}</small>
        </p>
      </div>
    </div>
  `,
  styleUrls: ['./post-view.component.css']
})
export class PostViewComponent implements OnInit {
  post$: Observable<Post>;

  constructor(private route: ActivatedRoute, private postsService: PostsService) {}

  ngOnInit() {
    this.post$ = this.route.paramMap.pipe(
      map(params => params.get('postId')),
      switchMap(postId => this.postsService.getPost(postId))
    );
  }
}
