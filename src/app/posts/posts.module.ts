import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { PostsRoutingModule } from './posts-routing.module';
import { PostsViewComponent } from './containers/posts-view/posts-view.component';
import { PostViewComponent } from './containers/post-view/post-view.component';

@NgModule({
  declarations: [PostsViewComponent, PostViewComponent],
  imports: [CommonModule, SharedModule, PostsRoutingModule]
})
export class PostsModule {}
