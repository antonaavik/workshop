import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersViewComponent } from './containers/users-view/users-view.component';
import { UserViewComponent } from './containers/user-view/user-view.component';

@NgModule({
  declarations: [UsersViewComponent, UserViewComponent],
  imports: [
    CommonModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
