import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

import { UsersService } from 'src/app/shared/services/users.service';

@Component({
  selector: 'app-user-view',
  template: `
    <div *ngIf="this.user$ | async as user">
      <img [src]="user.avatar" alt="" />
      <h3>{{ user.firstName }} {{ user.lastName }}</h3>
      <p>{{ user.position }}</p>
      <p>{{ user.birthDay | date }}</p>
    </div>
  `,
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {
  user$: Observable<User>;

  constructor(private route: ActivatedRoute, private usersService: UsersService) {}

  ngOnInit() {
    this.user$ = this.route.paramMap.pipe(
      map(params => params.get('userId')),
      switchMap(userId => this.usersService.getUser(userId))
    );
  }
}
