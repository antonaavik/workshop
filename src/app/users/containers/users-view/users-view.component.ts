import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService } from 'src/app/shared/services/users.service';

@Component({
  selector: 'app-users-view',
  template: `
    <h1>Users</h1>
    <ul>
      <li *ngFor="let user of users$ | async">
        <a href="#" (click)="showUser($event, user.id)">{{ user.firstName }}</a>
      </li>
    </ul>
  `,
  styleUrls: ['./users-view.component.css']
})
export class UsersViewComponent implements OnInit {
  users$: Observable<User[]>;

  constructor(private router: Router, private usersService: UsersService) {}

  ngOnInit() {
    this.users$ = this.usersService.getUsers();
  }

  showUser(e: any, userId: number) {
    e.preventDefault();
    this.router.navigate([`users/${userId}`]);
  }
}
