import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersViewComponent } from './containers/users-view/users-view.component';
import { UserViewComponent } from './containers/user-view/user-view.component';

const routes: Routes = [
  {
    path: '',
    component: UsersViewComponent
  },
  {
    path: ':userId',
    component: UserViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
