import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService } from '../../../shared/services/posts.service';

@Component({
  selector: 'app-home-view',
  template: `
    <h1>Home</h1>
    <div *ngFor="let post of posts$ | async">
      <app-card-post [post]="post"></app-card-post>
    </div>
  `,
  styleUrls: ['./home-view.component.css']
})
export class HomeViewComponent implements OnInit {
  posts$: Observable<Post[]>;

  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.posts$ = this.postsService.getPosts();
  }
}
