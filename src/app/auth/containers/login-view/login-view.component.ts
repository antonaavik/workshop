import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsersService } from 'src/app/shared/services/users.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-login-view',
  template: `
    <h1>Select user</h1>
    <form (submit)="handleSubmit($event)">
      <p>
        <select (change)="onChange($event)">
          <option></option>
          <option *ngFor="let user of users$ | async" [value]="user.id">{{
            user.firstName
          }}</option>
        </select>
      </p>
      <p><button type="submit" [disabled]="!selectedUser">Login</button></p>
    </form>
  `,
  styleUrls: ['./login-view.component.css']
})
export class LoginViewComponent implements OnInit {
  users$: Observable<any>;

  selectedUser: string;

  constructor(private authService: AuthService, private usersService: UsersService) {}

  ngOnInit() {
    this.users$ = this.usersService.getUsers();
  }

  onChange(e: any) {
    this.selectedUser = e.target.value;
  }

  handleSubmit(e: any) {
    this.authService.login(this.selectedUser);
  }
}
