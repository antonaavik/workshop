interface User {
  id: number;
  firstName: string;
  lastName: string;
  position: string;
  birthDay: string;
}

interface Post {
  id: number;
  cover: string;
  title: string;
  body: string;
  authors: User[];
  comments?: Comment[];
}

interface Comment {
  id: number;
  postId: number;
  body: string;
  author: User;
}
