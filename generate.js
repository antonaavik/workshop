module.exports = function() {
  var faker = require('faker');
  var _ = require('lodash');

  var users = _.times(100, function(n) {
    return {
      id: n,
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      avatar: faker.internet.avatar(),
      position: faker.name.jobTitle(),
      birthDay: faker.date.past(30)
    };
  });

  return {
    users: users,
    posts: _.times(20, function(n) {
      var randomUsersLength = Math.floor(Math.random() * 4);
      var authors = [];
      for (let i = 0; i <= randomUsersLength; i++) {
        var randomUserId = Math.floor(Math.random() * users.length);
        authors.push(users[randomUserId]);
      }
      return {
        id: n,
        title: faker.lorem.words(Math.floor(Math.random() * 5)),
        body: faker.lorem.words(30),
        authors: authors,
        cover: faker.image.nature(640, 320)
      };
    }),
    comments: _.times(50, function(n) {
      var randomUserId = Math.floor(Math.random() * users.length);
      return {
        id: n,
        body: faker.lorem.words(10),
        postId: Math.floor(Math.random() * 20),
        author: users[randomUserId]
      };
    })
  };
};
